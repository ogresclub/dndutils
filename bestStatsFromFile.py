import getopt
import sys
import argparse
from operator import attrgetter

###################### VALUES WHICH NEED TO BE DEFINED ########################

# the total amount of points are the same amount as if you buy from this value
# (in classic point buy you have to buy your stats from 8)
POINT_BUY_START = 8

#######################################################################

REFERENCE_STATS = [POINT_BUY_START, POINT_BUY_START, POINT_BUY_START, POINT_BUY_START, POINT_BUY_START, POINT_BUY_START]


class DndStats:

	def __init__(self, index, stats, WANTED_STATS, RACE_MODIFICATIONS):
		self.race_modifications_list = RACE_MODIFICATIONS
		self.index = index + 2
		self.stats = stats
		self.statsModifiedByRace = modifyStatsByRace(stats, RACE_MODIFICATIONS)
		self.pointsNeededToGetWantedStats = calcPointsFromHaveToWanted(WANTED_STATS, self.statsModifiedByRace)
		self.totalPointsWithRace = calcTotalPoints(self.statsModifiedByRace)
		self.totalPointsNoRace = calcTotalPoints(stats)

	def makeText(self):
		print('{} stats:{}, points need for Wanted Stats={}, total points={}/{}, racemods={}'.format(self.index,
																									 self.statsModifiedByRace,
																									 self.pointsNeededToGetWantedStats,
																									 self.totalPointsNoRace,
																									 self.totalPointsWithRace,
																									 self.race_modifications_list))


def sign(x):
	return 1 - 2 * (x <= 0)


def modifyStatsByRace(list, RACE_MODIFICATIONS):
	return [x + y for x, y in zip(list, RACE_MODIFICATIONS)]


def calcPoints(start, end):
	points = 0
	for i in range(start, end):
		j = i - 10
		if j >= 0:
			pointsNeededToIncreaseByOne = max(1, j // 2)
		else:
			pointsNeededToIncreaseByOne = -(j // 2)
		points += pointsNeededToIncreaseByOne
	return points


def calcPointsFromHaveToWanted(wanted, have):
	points = 0
	for i in range(max(len(wanted), len(have))):
		if wanted[i] > have[i]:
			points += calcPoints(have[i], wanted[i])
	return points


def calcTotalPoints(stats):
	points = 0
	for i in range(max(len(stats), len(REFERENCE_STATS))):
		refStat = REFERENCE_STATS[i]
		stat = stats[i]
		if refStat < stat:
			points += calcPoints(refStat, stat)
		elif refStat > stat:
			points -= calcPoints(stat, refStat)
	return points


def getStatList(FILE_NAME, WANTED_STATS, RACE_MODIFICATIONS, RACE_IS_HUMAN):
	with open(FILE_NAME) as file:
		statsarraywithshit = file.read().split('\n')
	stats = []
	nb_lines = len(statsarraywithshit)
	statList = []
	statsarray = []
	for x in statsarraywithshit:
		try:
			statsarray.append(int(x))
		except ValueError:
			# print('{}: Not a int'.format(x))
			nb_lines -= 1
	stats.append(0)
	# this will be popped later again, but we need to add it here because in the first round of the loop we dont
	# rally want to pop the first value
	if RACE_IS_HUMAN:
		# try all possible modifications for human to find the best one.
		race_mods_backup = RACE_MODIFICATIONS.copy()
		# check which index of RACE_MODIFICATIONS is not 0, because on this stat we cant apply the human +1 bonus
		alreadyHasBonus = []
		for k in range(6):
			if race_mods_backup[k] > 0:
				alreadyHasBonus.append(k)

	for i in range(5):
		stats.append(statsarray[i])

	for i in range(nb_lines - 5):
		stats.pop(0)
		# this kills the first entry and shifts all the other indices down by one
		stats.append(statsarray[i + 5])
		# print(i)
		# print('stat points = {}'.format(calcTotalPoints(stats)))
		if not RACE_IS_HUMAN:
			statList.append(DndStats(i, stats, WANTED_STATS, RACE_MODIFICATIONS))
		else:
			for m in range(6):
				for n in range(m + 1, 6):
					if (n not in alreadyHasBonus) and (m not in alreadyHasBonus):
						RACE_MODIFICATIONS = race_mods_backup.copy()
						RACE_MODIFICATIONS[m] = 1
						RACE_MODIFICATIONS[n] = 1
						statList.append(DndStats(i, stats, WANTED_STATS, RACE_MODIFICATIONS))

	return statList


def printBestStats(FILE_NAME, THRESHOLD, WANTED_STATS, AMOUNT_OF_RESULTS_SHOWN, RACE_MODIFICATIONS, RACE_IS_HUMAN):
	statList = getStatList(FILE_NAME, WANTED_STATS, RACE_MODIFICATIONS, RACE_IS_HUMAN)
	statList = sorted(statList, key=attrgetter('pointsNeededToGetWantedStats'))  # sort by pointsNeededToGetWantedStats
	statList = sorted(statList, key=attrgetter('totalPointsWithRace'), reverse=True)  # sort by totalPoints

	# print("stat list size = %d" % len(statList))
	if THRESHOLD == -1:
		# this means that no threshold was given, so we will simply print the best 5 stats
		for i in range(AMOUNT_OF_RESULTS_SHOWN):
			statList[i].makeText()
	else:
		# print all stats with pointsNeededToGetWantedStats < threshold
		for stat in statList:
			bestpoints = min([stats.pointsNeededToGetWantedStats for stats in statList])
			if stat.pointsNeededToGetWantedStats <= bestpoints + THRESHOLD:
				stat.makeText()


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='find the best set of stats in a list')
	parser.add_argument('racemodifications', help='the modifications given by your race.',
						nargs=6, type=int, metavar="racemod")
	parser.add_argument('-i', '--inputfile', help='the file where the stats will be taken from', default='stats.txt')
	parser.add_argument('-w', '--wantedstats', help='wanted stats as list, DEFAULT is all 20', nargs=6, type=int,
						default=[20, 20, 20, 20, 20, 20], metavar=("STR", "DEX", "CON", "INT", "WIS", "CHA"))
	parser.add_argument('-t', '--threshold', type=int, default=-1,
						help="if you enter a threshold, all stats that require less than (the optimums stats "
							 "- threshold) points to achieve wanted stats, will be printed")
	parser.add_argument('-n', '--numberofstatsshown', type=int, default=5,
						help="the number of best stats to show. This only has effect if no threshold is given")
	parser.add_argument('--human', action='store_true',
						help="use when your race is human and you also want to find the best stat to put you +1 human "
							 "bonus in. You still have to give a race modifications arg for the human +2 bonus")
	opts = parser.parse_args()
	printBestStats(opts.inputfile,
				   opts.threshold,
				   opts.wantedstats,
				   opts.numberofstatsshown,
				   opts.racemodifications,
				   opts.human)
