import shelve

from dataclasses import dataclass


def edit_attributes(item, opts, indices):
    """
    edit all attributes of the item according to the same attributes of opts

    :keyword indices the list of indices in opts.__dict__.values() that we want to use
    """
    for idx in indices:
        attr = list(opts.__dict__.keys())[idx]
        if getattr(item, attr) is not None:
            setattr(item, attr, getattr(opts, attr))


@dataclass
class ShelveClass:

    name: str
    # @staticmethod
    # @abc.abstractmethod
    # def from_parser(opts):
    #     """create a new object from a dict or opts from a argument parser"""
    #
    # @staticmethod
    # def from_file(path):
    #     """load data from file and create instance of class with this data"""
    #     with open(path, "r") as file:
    #         print(f"data: {file.read()}")
    #         return json.loads(file.read(), object_hook=JsonClass.from_parser, encoding="utf-8")
    #
    # def to_json_string(self):
    #     """create json string to store in file"""
    #     return json.dumps(self.__dict__, indent=4)
    #
    # def write_to_file(self, file_path):
    #     with open(file_path, "w") as file:
    #         file.write(self.to_json_string())

    @staticmethod
    def get(items: list, name: str):
        """return the JsonClass instance with given name (ignoring cases) or None if no such exists"""
        return next((i for i in items if i.name.casefold() == name.casefold()), None)

    @classmethod
    def get_all(cls, shelve_path):
        """return a list of all Instances of the class stored in shelve"""
        s = shelve.open(shelve_path)
        key = cls.__name__
        return s[key]

    def store_in_shelve(self, shelve_path, force_overwrite=False):
        """store the object in shelve to retrieve it later again"""
        key = self.__class__.__name__
        s = shelve.open(shelve_path)
        if key not in s.keys():
            s[key] = []
        items = s[key]
        item = ShelveClass.get(items, self.name)

        if item is None:
            items.append(self)
            s[key] = items
        else:
            if not force_overwrite:
                print(f"{self.__class__.__name__} with name {self.name} already exists")
                print("Overwrite?")
                conf = input()
                if not force_overwrite and conf.lower().startswith("n"):
                    return
                else:
                    item = self
        s.close()

    @classmethod
    def from_shelve(cls, shelve_path, name):
        """
        load a object from shelve, where it is stored
        return the first object with the given name or None if no object with this name exists
        """
        s = shelve.open(shelve_path)
        key = cls.__name__
        items = s[key]
        s.close()
        return next((i for i in items if i.name == name), None)

    @classmethod
    def delete(cls, shelve_path: str, name: str) -> bool:
        """
        delete an object from the shelve
        return True if item was deleted successfully
        """
        key = cls.__name__
        s = shelve.open(shelve_path)
        items = s[key]
        item = next((i for i in items if i.name == name), None)
        if item is None:
            print(f"Nothing to remove: {key} with name {name} was not found")
            s.close()
            return False
        else:
            try:
                items.remove(item)
                print(f"deleted: {item}")
                s[key] = items
                s.close()
                return True
            except ValueError:
                print(f"Oops. This should not have happened: {key} with name {name} was not found")
                s.close()
                return False


@dataclass
class Character(ShelveClass):
    """the data of a character"""
    attack: int
    damage: float
    # extra damage is not multiplied on a critical hit
    extra_damage: float
    crit_range: int
    critical_multiplier: int
    crit_confirm_bonus: int
    max_power_attack: int

    """ 
    the last two parameters can be related to a character, but are maybe not fixed, because riddle of steel or
    we want to test deadly defense and use a appropriate power attack multiplier to simulate the feat
    """
    power_attack_multiplier: int
    attacks_per_round: int

    @staticmethod
    def from_parser(opts):
        """create a Character with opts parse from arg_parser with Character argumnents"""
        return Character(
            name=opts.name,
            attack=opts.attack,
            damage=opts.damage,
            extra_damage=opts.extra_damage,
            crit_range=opts.crit_range,
            critical_multiplier=opts.crit_mult,
            crit_confirm_bonus=opts.crit_confirm_bonus,
            max_power_attack=opts.maximum_power_attack,
            power_attack_multiplier=opts.power_attack_multiplier,
            attacks_per_round=opts.attacks_per_round)


@dataclass
class Foe(ShelveClass):
    ac: int
    crit_ac: int
    dr: int

    @staticmethod
    def from_parser(opts):
        """create a Foe with opts parse from arg_parser with Foe argumnents"""
        return Foe(name=opts.name,
                   ac=opts.ac,
                   dr=opts.dr,
                   crit_ac=opts.crit_ac)