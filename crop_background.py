import os
import sys
from tkinter.dnd import dnd_start

from PIL import Image
from itertools import product


def tile(filename: str, number_of_pices: int, ratio: float = 20/9, cut_height: bool = False):
    dir_in = os.path.dirname(os.path.realpath(__file__))
    dir_out = dir_in
    name, ext = os.path.splitext(filename)
    img = Image.open(os.path.join(dir_in, filename))
    w, h = img.size
    d: int = int(w / number_of_pices)
    if ratio > 0:
        d = int(h / ratio)
        if number_of_pices * d > w:
            if not cut_height:
                print("image not wide enough."
                      "\nIf you want to neglect the ratio and only split the image "
                      "in the given amount of pieces, try using -1 for ration"
                      "\n if you want to cut some height of the image and force the ration use ! at the end of the ratio")
            else:
                H = int(w * ratio / number_of_pices)
                offset = int((h - H)/2)
                # print(f"h = {h}, H = {H}, offset = {offset}, low = {offset}, high = {h - offset}")
                img = img.crop((0, offset ,w, h - offset))
                h = H
                d = int(h / ratio)
        else:
            img = img.crop((0, 0, int(number_of_pices * d), h))

    grid = product([0], range(0, w - w % d, d))
    k = 0
    for i, j in grid:
        box = (j, i, j + d, i + h)
        out = os.path.join(dir_out, f'{name}_{k}{ext}')
        img.crop(box).save(out)
        k = k+1


if __name__ == '__main__':
    if len(sys.argv) > 2:
        filename = sys.argv[1]
        number = int(sys.argv[2])
        if len(sys.argv) == 4:
            ratio = sys.argv[3]
            cut_height = ratio.count("!")  > 0
            # print(f"cutting height: {cut_height}")
            nomidx = ratio.find("/")
            denomidx = nomidx + 1
            denom = ratio[denomidx:]
            if cut_height:
                denom = denom[:-1]
            if nomidx > -1:
                ratio_float = int(ratio[:nomidx]) / int(denom)
            else:
                # assume argument is integer (e.g. -1)
                ratio_float = int(ratio)

            tile(filename=filename, number_of_pices=number, ratio=ratio_float, cut_height=cut_height)
        else:
            tile(filename=filename, number_of_pices=number)
    else:
        print("Usage:",
              "\tpython3 crop_background.py [FILENAME] [NUMBER] [RATIO]",
              "\t[FILENAME]: the name of the file ",
              "\t[NUMBER]: the number of pieces you want to cut the file into (width)"
              "\t[RATIO]: the ratio of the created images (default 19/9 (S10e screen)). use -1 to simply split the image in pieces. add ! at the end to cut image height to make the ration possible.",
              sep="\n")


