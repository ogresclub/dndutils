import json
import shlex

from cmd2 import Cmd, with_argparser
from argparse import ArgumentParser
from data import *
from calcs import *
from arg_parsers import *


class CalculatorShell(Cmd):
    """opens a shell where you can use commands to do your calculation"""
    prompt = "calc>"
    shelve_path = "shelve"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Setting this true makes it run a shell command if a cmd2/cmd command doesn't exist
        self.default_to_shell = True
        self.debug = True

    # ####### CREATE #########

    character_parser = ArgumentParser(
        description='store character data')
    character_parser.add_argument('name', help="the name of the character")
    add_character_data_positional_arguments(character_parser)

    @with_argparser(character_parser)
    def do_create_character(self, opts):
        c = Character.from_parser(opts)
        c.store_in_shelve(shelve_path=CalculatorShell.shelve_path)
        print(f"created: {c}")

    foe_parser = ArgumentParser(
        description='store foe data')
    foe_parser.add_argument('name', help="the name of the foe")
    add_foe_data_positional_arguments(foe_parser)

    @with_argparser(foe_parser)
    def do_create_foe(self, opts):
        f = Foe.from_parser(opts)
        f.store_in_shelve(shelve_path=CalculatorShell.shelve_path)
        print(f"created: {f}")

    # ########## EDIT ###############

    edit_foe_parser = ArgumentParser(
        description='edit data of stored foe')
    edit_foe_parser.add_argument('name', help="the name of the foe you want to edit")
    add_foe_data_flag_arguments(edit_foe_parser)

    @with_argparser(edit_foe_parser)
    def do_edit_foe(self, opts):
        f: Foe = Foe.from_shelve(CalculatorShell.shelve_path, opts.name)
        if f is None:
            print(f"No Foe named {opts.name} found.")
            return
        else:
            edit_attributes(f, opts, range(1, 4))
            f.store_in_shelve(CalculatorShell.shelve_path, force_overwrite=True)
            print(f"updated: {f}")

    def complete_edit_foe(self, text, line, begidx, endidx):
        index_dict = {
            1: [f.name for f in Foe.get_all(CalculatorShell.shelve_path)]
        }

        # get foe with entered name if possible
        args = shlex.split(line)

        flag_dict = {"-arschlecken": "nixda"}
        if len(args) > 1:
            name = args[1]
            f: Foe = Foe.from_shelve(CalculatorShell.shelve_path, name)
            if f is not None:
                flag_dict = {
                    '-ac': [str(f.ac)],
                    '--armor_class': [str(f.ac)],
                    'crit_ac': [str(f.crit_ac)],
                    '-dr': [str(f.dr)],
                    '--damage_reduction': [str(f.dr)]
                }
        return self.index_based_complete(text, line, begidx, endidx, index_dict=index_dict,
                                         all_else=self.flag_based_complete(text, line, begidx, endidx,
                                                                           flag_dict=flag_dict))

    edit_character_parser = ArgumentParser(
        description='edit data of stored character')
    edit_character_parser.add_argument('name', help="the name of the character you want to edit")
    add_character_data_flag_arguments(edit_character_parser)

    @with_argparser(edit_character_parser)
    def do_edit_character(self, opts):
        c: Character = Character.from_shelve(CalculatorShell.shelve_path, opts.name)
        if c is None:
            print(f"No Character named {opts.name} found.")
            return
        else:
            edit_attributes(c, opts, range(1, 10))
            c.store_in_shelve(CalculatorShell.shelve_path, force_overwrite=True)
            print(f"updated: {c}")

    def complete_edit_character(self, text, line, begidx, endidx):
        index_dict = {
            1: [c.name for c in Character.get_all(CalculatorShell.shelve_path)]
        }

        # get character with entered name if possible
        args = shlex.split(line)

        flag_dict = {"-arschlecken": "nixda"}
        if len(args) > 1:
            name = args[1]
            c: Character = Character.from_shelve(CalculatorShell.shelve_path, name)
            if c is not None:
                flag_dict = {
                    '-a': [str(c.attack)],
                    '--attack': [str(c.attack)],
                    '-d': [str(c.damage)],
                    '--damage': [str(c.damage)],
                    '-ed': [str(c.extra_damage)],
                    '--extra_damage': [str(c.extra_damage)],
                    '-cr': [str(c.crit_range)],
                    '--crit_range': [str(c.crit_range)],
                    '-cm': [str(c.critical_multiplier)],
                    '--crit_mult': [str(c.critical_multiplier)],
                    '-apr': [str(c.attacks_per_round)],
                    '--attacks_per_round': [str(c.attacks_per_round)],
                    '-maxpwr': [str(c.max_power_attack)],
                    '--maximum_power_attack': [str(c.max_power_attack)],
                    '-pam': [str(c.power_attack_multiplier)],
                    '--power_attack_multiplier': [str(c.power_attack_multiplier)],
                    '-ccf': [str(c.crit_confirm_bonus)],
                    '--crit_confirm_bonus': [str(c.crit_confirm_bonus)],
                }
        return self.index_based_complete(text, line, begidx, endidx, index_dict=index_dict,
                                         all_else=self.flag_based_complete(text, line, begidx, endidx,
                                                                           flag_dict=flag_dict))

    ########## DELETE #############

    delete_character_parser = ArgumentParser(
        description='delete a character from storage')
    delete_character_parser.add_argument('name', help="the name of the character you want to delete")

    @with_argparser(delete_character_parser)
    def do_delete_character(self, opts):
        Character.delete(CalculatorShell.shelve_path, opts.name)

    def complete_delete_character(self, text, line, begidx, endidx):
        index_dict = {
            1: [c.name for c in Character.get_all(CalculatorShell.shelve_path)]
        }
        return self.index_based_complete(text, line, begidx, endidx, index_dict=index_dict)

    delete_foe_parser = ArgumentParser(
        description='delete a foe from storage')
    delete_foe_parser.add_argument('name', help="the name of the foe you want to delete")

    @with_argparser(delete_foe_parser)
    def do_delete_foe(self, opts):
        Foe.delete(CalculatorShell.shelve_path, opts.name)

    def complete_delete_foe(self, text, line, begidx, endidx):
        index_dict = {
            1: [f.name for f in Foe.get_all(CalculatorShell.shelve_path)]
        }
        return self.index_based_complete(text, line, begidx, endidx, index_dict=index_dict)

    ########## SHOW EXISTING #############

    do_chars_parser = ArgumentParser(description="show all stored characters")
    do_chars_parser.add_argument("-v", "--verbose", action="store_true",
                                 help="show complete data of all chars")

    @with_argparser(do_chars_parser)
    def do_chars(self, opts):
        if opts.verbose:
            for c in Character.get_all(CalculatorShell.shelve_path):
                print(c)
        else:
            print([item.name for item in Character.get_all(CalculatorShell.shelve_path)])

    do_foes_parser = ArgumentParser(description="show all stored foes")
    do_foes_parser.add_argument("-v", "--verbose", action="store_true",
                                 help="show complete data of all foes")

    @with_argparser(do_foes_parser)
    def do_foes(self, opts):
        if opts.verbose:
            for f in Foe.get_all(CalculatorShell.shelve_path):
                print(f)
        else:
            print([item.name for item in Foe.get_all(CalculatorShell.shelve_path)])

    calc_optimized_damage_parser = ArgumentParser(
        description='calculate average dmg with given parameters and optimised power attack')
    calc_optimized_damage_parser.add_argument("character", help="the name of the character")
    calc_optimized_damage_parser.add_argument("foe", help="the name of the foe")

    @with_argparser(calc_optimized_damage_parser)
    def do_optimized_damage(self, opts):
        c = Character.from_shelve(shelve_path=CalculatorShell.shelve_path, name=opts.character)
        f = Foe.from_shelve(shelve_path=CalculatorShell.shelve_path, name=opts.foe)
        print(optimized_average_damage(character=c, foe=f))

    def complete_optimized_damage(self, text, line, begidx, endidx):
        index_dict = {
            1: [c.name for c in Character.get_all(CalculatorShell.shelve_path)],
            2: [f.name for f in Foe.get_all(CalculatorShell.shelve_path)]
        }
        return self.index_based_complete(text, line, begidx, endidx, index_dict=index_dict)


if __name__ == "__main__":
    CalculatorShell().cmdloop()
    # plot(character=c, foe=f)
