import subprocess

from data import Character, Foe


def half(number, threshold):
    """ half number above given threshold """

    if number <= threshold:
        return number
    else:
        return (number - threshold) // 2 + threshold


def hit_chance(attack, ac):
    """ chance to hit with an attack """
    # todo -+1d6 on 1 and 20?
    roll_needed = max((0, ac - attack))
    return (20 - roll_needed) / 20


def crit_chance(crit_range: int, crit_attack_bonus: int, crit_ac: int):
    crit_confirm_chance = hit_chance(crit_attack_bonus, crit_ac)
    return (20 - crit_range + 1) / 20 * crit_confirm_chance


def average_damage_variable_return_all_data(power: int,
                                            character: Character,
                                            foe: Foe,
                                            power_attack_multiplier: int,
                                            attacks_per_round: int):
    """
    calculate average damage per round with given parameters
    return all kinds of useful data generated on the way
    """
    attack = half(character.attack - power, 20)
    hitchance = hit_chance(attack, foe.ac)
    critchance = crit_chance(character.crit_range, attack + character.crit_confirm_bonus, max(foe.crit_ac, foe.ac))
    base_damage_per_hit: float = character.damage + power * power_attack_multiplier
    damage_on_normal_hit = base_damage_per_hit + float(character.extra_damage)  - foe.dr
    dmg = attacks_per_round * damage_on_normal_hit * hitchance
    dmg += attacks_per_round * critchance * (character.critical_multiplier - 1) * base_damage_per_hit

    # interesting data not really needed
    expected_hits = hitchance * attacks_per_round
    extra_dmg_from_power = power * power_attack_multiplier * attacks_per_round
    expected_dmg_per_hit_incl_crits = dmg / attacks_per_round
    extra_dmg_from_crit_per_hit = critchance * (character.critical_multiplier - 1) * base_damage_per_hit

    return dmg, attack, hitchance, critchance, expected_hits, extra_dmg_from_power, damage_on_normal_hit, extra_dmg_from_crit_per_hit, expected_dmg_per_hit_incl_crits


def average_damage_variable(power: int,
                            character: Character,
                            foe: Foe,
                            power_attack_multiplier: int,
                            attacks_per_round: int):
    """calculate average damage per round with given parameters"""
    result, _, _, _, _, _, _, _, _ = average_damage_variable_return_all_data(power, character, foe, power_attack_multiplier, attacks_per_round)
    return


def average_damage(power: int,
                   character: Character,
                   foe: Foe):
    return average_damage_variable(power, character, foe, character.power_attack_multiplier,
                                   character.attacks_per_round)


def optimized_average_damage(character: Character, foe: Foe, ac=None, dr=None, crit_ac=None) -> tuple:
    """
    average damage per round with optimal power attack
    returns tuple (damage, power attack used)
    """
    if ac is None: ac = foe.ac
    if dr is None: dr = foe.dr
    if crit_ac is None: crit_ac = foe.crit_ac
    damages = []
    for pa in range(character.max_power_attack):
        damages.append((
            average_damage(power=pa,
                           character=character,
                           foe=Foe(foe.name, ac, crit_ac, dr)),
            pa))
    return max(damages, key=lambda x: x[0])


def plot(character: Character, foe: Foe, min_plot=10, max_plot=50, ac=True, dr=False, crit_ac=False,
         filename="plot.txt", plot=True):
    """ writes average damage over either ac, dr or crit ac to a file for plotting purposes. """
    with open(filename, "w") as output:
        for x in range(min_plot, max_plot):
            if ac:
                dmg = optimized_average_damage(character=character, foe=foe, ac=x)
            elif dr:
                dmg = optimized_average_damage(character=character, foe=foe, dr=x)
            else:
                dmg = optimized_average_damage(character=character, foe=foe, crit_ac=x)
            output.write("%d\t%.2f\n" % (x, dmg[0]))
    if plot:
        subprocess.call("gnuplot -e \"set title 'AC plot'; plot '%s';\" -p " % filename, shell=True)