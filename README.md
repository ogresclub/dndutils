# dndutils
## Damage Calculator
- Calculates average damage per round for given attack, damage and opponent stats.
- Optimizes power Attack usage.
- Plots damage over ac.

## Stats Roller
- Generates character stats for given seed.

## Best Stats From File
- Searches for best stats in output of Stats Roller.
- Supports race modifications and wanted stats.

## Crop Background
- cut image horizontally in equal pieces. (chartracker backgrounds)

## Skill Resources Creator
- create resources for chartracker for given skill list with resources and types