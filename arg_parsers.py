from argparse import ArgumentParser


def add_character_data_positional_arguments(parser: ArgumentParser):
    """
    add positional arguments to the parser which are needed to create a Character object
    except for name
    """
    # character stats
    parser.add_argument('attack', type=int,
                        help='attack bonus, NOT HALVED ABOVE 20!!')
    parser.add_argument('damage', type=float,
                        help='the (average) damage of each attack')
    parser.add_argument('extra_damage', help='damage which is not multiplied on a critical hit')
    parser.add_argument('crit_range', type=int,
                        help='e.g. x for threat range x - 20')
    parser.add_argument('critical_multiplier', type=int,
                        help='the factor with which the damage is multiplied on a critical hit')
    parser.add_argument('attacks_per_round', type=int,
                        help='the number of attacks you want to calculate the average dmg for')

    # optional arguments
    parser.add_argument('-maxpwr', '--max_power_attack', type=int, default=7,
                        help='DEFAULT 7, the maximum amount of power attack you can do')
    parser.add_argument('-pam', '--power_attack_multiplier', type=int, default=1,
                        help='DEFAULT 1, for -x attack you receive + power attack multiplier * x damage')
    parser.add_argument('-ccf', '--crit_confirm_bonus', type=int, default=0,
                        help='DEFAULT 0, attack bonus on crit confirms')


def add_character_data_flag_arguments(parser: ArgumentParser):
    """
    add flag arguments to the parser which are needed to create a Character object
    except for name
    """
    # character stats
    parser.add_argument('-a', '--attack', type=int,
                        help='attack bonus, NOT HALVED ABOVE 20!!')
    parser.add_argument('-d', '--damage', type=float,
                        help='the (average) damage of each attack')
    parser.add_argument('-ed', '--extra_damage', help='damage which is not multiplied on a critical hit')
    parser.add_argument('-cr', '--crit_range', type=int,
                        help='e.g. x for threat range x - 20')
    parser.add_argument('-cm', '--critical_multiplier',
                        help='the factor with which the damage is multiplied on a critical hit')
    parser.add_argument('-apr', '--attacks_per_round', type=int, metavar="attacks_per_round",
                        help='the number of attacks you want to calculate the average dmg for')

    # optional arguments
    parser.add_argument('-maxpwr', '--max_power_attack', type=int,
                        help='the maximum amount of power attack you can do')
    parser.add_argument('-pam', '--power_attack_multiplier', type=int,
                        help='for -x attack you receive + power attack multiplier * x damage')
    parser.add_argument('-ccf', '--crit_confirm_bonus', type=int,
                        help='attack bonus on crit confirms')


def add_foe_data_positional_arguments(parser: ArgumentParser):
    """
    add positional arguments to the parser which are needed to create a Foe object
    except for name
    """
    # enemy stats
    parser.add_argument('ac', type=int, nargs="?", default=30,
                        help='DEFAULT 30, the armor class of your dummy target, halved above 30')
    parser.add_argument('crit_ac', type=int, nargs="?", default=30,
                        help='DEFAULT 30, the armor class against critical hits of the dummy target, halved above 30')
    parser.add_argument('dr', type=int, nargs="?", default=0,
                        help='DEFAULT 0, the damage reduction of your dummy target')


def add_foe_data_flag_arguments(parser: ArgumentParser):
    """
    add flag arguments to the parser which are needed to create a Foe object
    except for name
    """
    # enemy stats
    parser.add_argument('-ac', '--armor_class', type=int, nargs="?", default=30,
                        help='DEFAULT 30, the armor class of your dummy target, halved above 30')
    parser.add_argument('-crit_ac', type=int, nargs="?", default=30,
                        help='DEFAULT 30, the armor class against critical hits of the dummy target, halved above 30')
    parser.add_argument('-dr', '--damage_reduction', type=int, nargs="?", default=0,
                        help='DEFAULT 0, the damage reduction of your dummy target')
