import random
import sys 
import argparse


def rollDice():
	"""roll 4 dice and take the sum of the three best as stat."""
	array = []
	for i in range(4):
		array += [random.randint(1,6)]
	array.sort(reverse=True)
	sum = 0
	for i in range(3):
		sum += array[i]
	return sum


def printAlot(FILE_NAME, SEED):
	"""roll 100 dices and print them"""
	with open(FILE_NAME,"w+") as f:
		random.seed(SEED)
		f.write("Seed = '{}', python version = {}".format(SEED, sys.version) + '\r\n')
		for i in range(999):
			f.write(str(rollDice()) + '\r\n')
		f.write(str(rollDice()))


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='generate 1000 stats with given seed and store them in file')
	parser.add_argument('seed', help='the seed for the random generator')
	parser.add_argument('-o', '--outputfile', help='the file where the stats will be written to', default='stats.txt')
	opts = parser.parse_args()
	printAlot(opts.outputfile, opts.seed)
