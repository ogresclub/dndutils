skills = [
    'Cracking',
    'Electronics',
    'Astral',
    'Athletics',
    'Summoning',
    'Biotech',
    'Influence',
    'Exotic Weapons',
    'Fire Arms',
    'Stealth',
    'Sorcery',
    'Engineering',
    'Melee Combat',
    'Outdoors',
    'Piloting',
    'Tasking',
    'Con',
    'Enchanting',
    'Perception'
]

attrDict = {
    'Cracking': 'Logic',
    'Electronics': 'Logic',
    'Astral': 'Intuition',
    'Athletics': 'Agility',
    'Summoning': 'Magic',
    'Biotech': 'Logic',
    'Influence': 'Charisma',
    'Exotic Weapons': 'Agility',
    'Fire Arms': 'Agility',
    'Stealth': 'Agility',
    'Sorcery': 'Magic',
    'Engineering': 'Logic',
    'Melee Combat': 'Agility',
    'Outdoors': 'Intuition',
    'Piloting': 'Reaction',
    'Tasking': 'Resonance',
    'Con': 'Charisma',
    'Enchanting': 'Magic',
    'Perception': 'Intuition'
}
skill_type_dict = {
}

default_skill_type = 'General'
rule_system_name = "shadowrun6"