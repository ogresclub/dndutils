attrDict = {
    'Athletics': 'Strength',
    'Acrobatics': 'Dexterity',
    'Sleight of Hand': 'Dexterity',
    'Stealth': 'Dexterity',
    'Arcana': 'Intelligence',
    'History': 'Intelligence',
    'Investigation': 'Intelligence',
    'Nature': 'Wisdom',
    'Religion': 'Intelligence',
    'Animal Handling': 'Dexterity',
    'Insight': 'Wisdom',
    'Medicine': 'Wisdom',
    'Perception': 'Wisdom',
    'Survival': 'Wisdom',
    'Deception': 'Charisma',
    'Intimidate': 'Charisma',
    'Performance': 'Charisma',
    'Persuasion': 'Charisma'
}
skill_type_dict = {
}

default_skill_type = 'General'
rule_system_name = "dnd_5e"