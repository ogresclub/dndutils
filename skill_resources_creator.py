attrDict = {
    'Athletics': 'Strength',
    'Acrobatics': 'Dexterity',
    'Sleight of Hand': 'Dexterity',
    'Stealth': 'Dexterity',
    'Arcana': 'Intelligence',
    'History': 'Intelligence',
    'Investigation': 'Intelligence',
    'Nature': 'Wisdom',
    'Religion': 'Intelligence',
    'Animal Handling': 'Dexterity',
    'Insight': 'Wisdom',
    'Medicine': 'Wisdom',
    'Perception': 'Wisdom',
    'Survival': 'Wisdom',
    'Deception': 'Charisma',
    'Intimidate': 'Charisma',
    'Performance': 'Charisma',
    'Persuasion': 'Charisma'
}
skill_type_dict = {
}

default_skill_type = 'General'
rule_system_name = "dnd_5e"


# add default skill type to list of all existing skill types
skill_types = list(skill_type_dict.values())
skill_types.append(default_skill_type)
skill_types = set(skill_types)

skills = set(attrDict.keys())


def create_string_resource(string_id: str, string: str) -> str:
    return f'<string name="{string_id}">{string}</string>\n'


def get_ref(s: str) -> str:
    return s.lower().replace(" ", "_")


def get_skill_type_id(skill_type_name: str) -> str:
    stref = get_ref(skill_type_name)
    return f'{rule_system_name}_skill_type_name_{stref}'


def get_attribute_id(attr_name: str) -> str:
    return f'attribute_{get_ref(attr_name)}'


def get_skill_id(skill_name: str) -> str:
    skref = get_ref(skill_name)
    return f'{rule_system_name}_skill_{skref}'


# create skill name strings
with open(f'{rule_system_name}_skills.xml', "w") as file:
    for skn in skills:
        skId = get_skill_id(skn)
        file.write(
            create_string_resource(skId, skn)
        )

    # create skill type name string
    for st in skill_types:
        stId = get_skill_type_id(st)
        file.write(
            create_string_resource(stId, st)
        )

    # create skill type names array
    all_skill_groups_array = f'<string-array name="{rule_system_name}skill_types">\n'
    for st in skill_types:
        all_skill_groups_array += f'\t<item>@string/{get_skill_type_id(st)}</item>\n'
    all_skill_groups_array += f'</string-array>\n'
    file.write(all_skill_groups_array)

    # create skill array for each skill
    for skn in skills:
        attrId = get_attribute_id(attrDict[skn])
        skgId = get_skill_type_id(skill_type_dict.get(skn, default_skill_type))
        skId = get_skill_id(skn)

        skillGroupAtStringRef = f'@string/{skgId}'
        if len(skgId) == 0:
            skillGroupAtStringRef = ""

        a = f'<string-array name="{skId}">\n'
        a += f'\t<item name="name">@string/{skId}</item>\n'
        a += f'\t<item name="attributes">@string/{attrId}</item>\n'
        a += f'\t<item name="type">{skillGroupAtStringRef}</item>\n'
        a += f'</string-array>\n'
        file.write(a)

    # create all skills array
    all_skills_array = f'<array name="{rule_system_name}_skill_list">\n'
    for skn in skills:
        print(skn)
        all_skills_array += f'\t<item>@array/{get_skill_id(skn)}</item>\n'
    all_skills_array += f'</array>\n'
    file.write(all_skills_array)

